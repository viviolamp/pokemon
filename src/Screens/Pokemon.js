import React, { Component } from "react";
import Post from "../Component/Post";
import { Row, Col } from "reactstrap";
import axios from "axios";

export default class Pokemon extends Component {
  constructor() {
    super();
    this.state = {
      pokemon: [],
      limit: 20,
      offset: 1,
    };
  }

  componentDidMount() {
    this.fetchPikaPokemon();
  }

  // fetchPikaPokemon = () => {
  //   const { limit, offset } = this.state;
  //   fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
  //     .then((response) => response.json())
  //     .then((data) => this.setState({ pokemon: data }));
  // };

  fetchPikaPokemon = () => {
    const { limit, offset } = this.state;
    axios
      .get(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
      .then((response) => this.setState({ pokemon: response.data }))
      .catch(function (error) {
        console.log(error);
      });
  };

  render() {
    const { pokemon } = this.state;
    return (
      <div className="allpokemon">
        <div className="pokemon ">
          <h1></h1>
          {pokemon.length === 0 ? null : (
            <Row className="p-3">
              {pokemon.results.map((result, index) => (
                <Col
                  sm="4"
                  md="3"
                  onClick={() =>
                    this.props.history.push(`/pokemon/${index + 1}`)
                  }
                >
                  <Post
                    title={result.name}
                    img={`https://pokeres.bastionbot.org/images/pokemon/${
                      index + 1
                    }.png`}
                  />
                </Col>
              ))}
            </Row>
          )}
        </div>
      </div>
    );
  }
}
