import React from "react";
import { Card, CardImg, CardBody, CardTitle } from "reactstrap";

export default function Post({ title, img }) {
  return (
    <div>
      <Card className="card">
        <CardImg top width="100%" src={img} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">{title}</CardTitle>
        </CardBody>
      </Card>
    </div>
  );
}
