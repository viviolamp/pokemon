import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Logo from "./Images/black2.jpg";
import NavbarStyled from "./Component/Navbar";
import Home from "./Screens/Home";
import Pokemon from "./Screens/Pokemon";

function App() {
  return (
    <Router>
      <div className="navigator">
        <div>
          <img className="m-2 p-2" width="50px" src={Logo} />
        </div>
        <NavbarStyled />
      </div>

      <Switch>
        <Route component={Home} path="/blog" />
        <Route component={Pokemon} path="/pokemon" />
      </Switch>
    </Router>
  );
}

export default App;
